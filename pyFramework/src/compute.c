#include <string.h>
#include "pyFramework_ds.h"


int32_t init_struct(main_t* a, unsigned int count)
{
    unsigned int i;

    if (count >= 16)
        return 1;

    a->size = count;
    for (i=0; i<count; i++) {
        a->items[i].thing = 2*i;
        a->items[i].other = 2*i+1;
    }
    return 0;
}

int32_t access_item(main_t* a, unsigned int idx, item_t *b)
{
    if (idx >= a->size || idx < 0)
        return 1;


    // b = &a->items[idx];
    memcpy(b, &(a->items[idx]), sizeof(item_t));

    return 0;
}

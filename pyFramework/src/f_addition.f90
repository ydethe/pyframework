! -ffree-form
! -ffree-line-length-none
! -fimplicit-none
! -fdefault-integer-8

! Generation .pyf:
! f2py -h pyFramework.pyf f_addition.f90

! Generation wrapper:
! f2py -m pyFramework pyFramework.pyf

! Generation dll:
! f2py -m f_pyFramework -c f_addition.f90


integer*4 function f_nb_items(a, size)
    include 'pyFramework_ds.finc'

    type(main_t), INTENT(IN) :: a
    integer*4, INTENT(OUT) :: size
    
    size = a%size
    f_nb_items = 0

end function f_nb_items

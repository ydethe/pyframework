import os
import sys
import unittest

sys.path.insert(0, os.path.dirname(__file__))
from TestBase import TestBase

from pyFramework.f_pyFramework import bar
import pyFramework.c_pyFramework as cm


class TestMain(TestBase):
    def test_fortran(self):
        count = 3
        b = cm.main_t()
        cm.foo(b, count)
        self.assertEqual(bar(b), count)

    def test_c(self):
        count = 3
        b = cm.main_t()
        cm.foo(b, count)
        a = cm.access_item(b, 1)
        self.assertEqual(a.thing, 2)
        self.assertEqual(a.other, 3)


if __name__ == "__main__":
    a = TestMain()
    a.test_cmd_mtcl()

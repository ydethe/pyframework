import os
import ctypes
import yaml
from subprocess import check_call

import networkx as nx
from networkx.algorithms.dag import topological_sort


_itf_types_map = {
    "uint8": "int",
    "uint16": "int",
    "uint32": "int",
    "uint64": "int",
    "int8": "int",
    "int16": "int",
    "int32": "int",
    "int64": "int",
    "float32": "float",
    "float64": "float",
}

_py_types_map = {
    "uint8": "ctypes.c_uint8",
    "uint16": "ctypes.c_uint16",
    "uint32": "ctypes.c_uint32",
    "uint64": "ctypes.c_uint64",
    "int8": "ctypes.c_int8",
    "int16": "ctypes.c_int16",
    "int32": "ctypes.c_int32",
    "int64": "ctypes.c_int64",
    "float32": "ctypes.c_float",
    "float64": "ctypes.c_double",
}

_c_types_map = {
    "uint8": "uint8_t",
    "uint16": "uint16_t",
    "uint32": "uint32_t",
    "uint64": "uint64_t",
    "int8": "int8_t",
    "int16": "int16_t",
    "int32": "int32_t",
    "int64": "int64_t",
    "float32": "float",
    "float64": "double",
}

_f_types_map = {
    "uint8": "integer*1",
    "uint16": "integer*2",
    "uint32": "integer*4",
    "uint64": "integer*8",
    "int8": "integer*1",
    "int16": "integer*2",
    "int32": "integer*4",
    "int64": "integer*8",
    "float32": "real*4",
    "float64": "real*8",
}


class Field(object):
    def __init__(self, name: str, typ: str, niter: int, unit: str, description: str):
        self.name = name
        self.typ = typ
        self.niter = niter
        self.unit = unit
        self.descrption = description

    def isBasicType(self) -> bool:
        return self.typ in _c_types_map.keys()

    def toCType(self) -> str:
        if self.isBasicType():
            ctyp = _c_types_map[self.typ]
        else:
            ctyp = self.typ

        if self.niter == 1:
            return "    %s %s;" % (ctyp, self.name)
        else:
            return "    %s %s[%i];" % (ctyp, self.name, self.niter)

    def toFType(self) -> str:
        if self.isBasicType():
            ctyp = _f_types_map[self.typ]
        else:
            ctyp = "type(%s)" % self.typ

        if self.niter == 1:
            return "    %s :: %s" % (ctyp, self.name)
        else:
            return "    %s :: %s(0:%i)" % (ctyp, self.name, self.niter - 1)

    def toPyType(self) -> str:
        if self.isBasicType():
            ctyp = "%s" % _py_types_map[self.typ]
        else:
            ctyp = self.typ

        if self.niter == 1:
            return '        ("%s", %s),' % (self.name, ctyp)
        else:
            return '        ("%s", %s*%i),' % (self.name, ctyp, self.niter)


class DataStructure(object):
    def __init__(self, name: str):
        self.name = name
        self.fields = []
        self.deps = []

    def addField(
        self, name: str, typ: str, niter: int, unit: str, description: str
    ) -> Field:
        field = Field(name, typ, int(niter), unit, description)
        self.fields.append(field)
        if not field.isBasicType():
            self.deps.append(field)
        return field

    def toCHeader(self) -> str:
        res = ["typedef struct"]
        res.append("{")
        for f in self.fields:
            res.append(f.toCType())
        res.append("} %s;" % self.name)
        return "\n".join(res)

    def toFHeader(self) -> str:
        res = ["type %s" % self.name]
        for f in self.fields:
            res.append(f.toFType())
        res.append("end type %s" % self.name)
        return "\n".join(res)

    def toPyHeader(self) -> str:
        res = ["class %s(ctypes.Structure):" % self.name]
        res.append("    _fields_ = [")
        for f in self.fields:
            res.append(f.toPyType())
        res.append("    ]")
        return "\n".join(res)

    @classmethod
    def fromFile(cls, file: str) -> str:
        with open(file, "r") as f:
            lines = f.readlines()

        fn = os.path.basename(file)
        ds_name, _ = os.path.splitext(fn)
        ds = cls(ds_name)
        for l in lines:
            if l.startswith("#"):
                continue
            elems = l.strip().split(",")
            if len(elems) != 5:
                break

            name, typ, niter, unit, description = [x.strip() for x in elems]

            field = ds.addField(name, typ, int(niter), unit, description)

        return ds


def generateTypeHeaders(root: str, out_file: str):
    G = nx.DiGraph()
    for dirpath, dirnames, filenames in os.walk(root):
        for f in filenames:
            _, ext = os.path.splitext(f)
            if ext != ".ds":
                continue

            ds = DataStructure.fromFile(os.path.join(dirpath, f))
            G.add_node(ds.name, ds=ds)
            for dep in ds.deps:
                G.add_edge(dep.typ, ds.name)

    list_ds = list(topological_sort(G))

    # Generation header C
    _, fn = os.path.split(out_file)
    macro_name = "_%s_H_" % fn.upper()
    res = ["// %s.h" % out_file, "", "#include <stdint.h>", "", ""]
    res.append("#ifndef %s" % macro_name)
    res.append("#define %s" % macro_name)
    res.append("")
    for ds in list_ds:
        res.append(G.nodes[ds]["ds"].toCHeader())
        res.append("")
    res.append("#endif")
    res.append("")

    f = open("%s.h" % out_file, "w")
    f.write("\n".join(res))
    f.close()

    # Generation header Fortran
    res = ["! %s.finc" % out_file]
    res.append("")
    for ds in list_ds:
        res.append(G.nodes[ds]["ds"].toFHeader())
        res.append("")

    f = open("%s.finc" % out_file, "w")
    f.write("\n".join(res))
    f.close()

    # Generation header Python
    res = ["# %s.py" % out_file]
    res.append("")
    res.extend(["import ctypes", "", ""])
    for ds in list_ds:
        res.append(G.nodes[ds]["ds"].toPyHeader())
        res.append("")

    f = open("%s.py" % out_file, "w")
    f.write("\n".join(res))
    f.close()


def loadFunctionDescription(fic: str) -> dict:
    with open(fic, "r") as f:
        dat = yaml.load(f, Loader=yaml.SafeLoader)
    return dat


def descToPython(dat: dict, pkg_name: str) -> str:
    code = ["import ctypes"]
    code.append("from ctypes import byref")
    code.append("from %s.include.%s_ds import *" % (pkg_name, pkg_name))
    code.append(
        """lib = ctypes.cdll.LoadLibrary("./%s/_%s.so")""" % (pkg_name, pkg_name)
    )

    for fname in dat.keys():
        # Building function declaration line,
        # and parsing arguments
        # -------------------------------
        line_def = """def %s(""" % fname
        l_arg_in = []
        for arg in dat[fname]["arguments"]:
            if arg["intent"] == "in":
                if arg["type"] in _itf_types_map.keys():
                    typ = _itf_types_map[arg["type"]]
                    ctyp = _py_types_map[arg["type"]]
                    byref = False
                else:
                    typ = arg["type"]
                    ctyp = ""
                    byref = True
                line_def += "%s: %s, " % (arg["name"], typ)
                l_arg_in.append((arg["name"], ctyp, byref))

        l_out = []
        l_arg_out = []
        for arg in dat[fname]["arguments"]:
            if arg["intent"] == "out":
                if arg["type"] in _itf_types_map.keys():
                    typ = _itf_types_map[arg["type"]]
                    ctyp = _py_types_map[arg["type"]]
                    create = False
                else:
                    typ = arg["type"]
                    ctyp = arg["type"]
                    create = True
                l_out.append(typ)
                l_arg_out.append((arg["name"], typ, ctyp, create))

        if len(l_out) == 0:
            line_def = line_def[:-2] + "):"
        elif len(l_out) == 1:
            line_def = line_def[:-2] + ") -> %s:" % l_out[0]
        else:
            line_def = line_def[:-2] + ") -> Tuple[%s]:" % (", ".join(l_out))
        code.append(line_def)
        code.append("    '''%s'''" % dat[fname]["help"])

        # Building local variables
        # -------------------------------
        for arg, typ, byref in l_arg_in:
            code.append("""    itf_%s = %s(%s)""" % (arg, typ, arg))

        for arg, typ, ctyp, create in l_arg_out:
            code.append("""    itf_%s = %s()""" % (arg, ctyp))

        # Calling the compiled function
        # -------------------------------
        if dat[fname]["langage"] == "F":
            line_call = "    res=lib.%s_(" % fname
        elif dat[fname]["langage"] == "C":
            line_call = "    res=lib.%s(" % fname
        else:
            raise ValueError(dat[fname]["langage"])

        for arg, typ, byref in l_arg_in:
            if byref:
                line_call += "byref(itf_%s), " % arg
            else:
                line_call += "itf_%s, " % arg

        for arg, typ, ctyp, create in l_arg_out:
            line_call += "byref(itf_%s), " % arg

        line_call = line_call[:-2] + ")"
        code.append(line_call)

        # Handling return code
        # -------------------------------
        code.append("    if res != 0:")
        code.append("        raise ValueError(res)")

        # Building return statement
        # -------------------------------
        if len(l_arg_out) != 0:
            line_ret = "    return "

            for arg, typ, ctyp, create in l_arg_out:
                if create:
                    line_ret += "itf_%s, " % arg
                else:
                    line_ret += "itf_%s.value, " % arg

            code.append(line_ret[:-2])

        code.append("")

    return "\n".join(code)


def generateFunctionHeaders(root: str, pkg_name: str, out_file: str):
    for dirpath, dirnames, filenames in os.walk(root):
        for f in filenames:
            _, ext = os.path.splitext(f)
            if ext != ".fd":
                continue

            fd_pth = os.path.join(dirpath, f)
            fcts = loadFunctionDescription(fd_pth)
            code = descToPython(fcts, pkg_name)
            f = open(out_file, "w")
            f.write(code)
            f.close()


def compile(
    compilator: str,
    sources: list,
    incdirs: list = [],
    destdir: str = "build",
    cflags: list = ["-fPIC"],
):
    s_incdirs = "-I ".join(incdirs)
    for source in sources:
        d, fn = os.path.split(source)
        bn, _ = os.path.splitext(fn)
        tgt = os.path.join(destdir, bn) + ".o"
        if os.path.exists(tgt):
            compile = os.stat(tgt).st_mtime_ns < os.stat(source).st_mtime_ns
        else:
            compile = True

        if compile:
            cmd = "%s %s -I %s -c %s  -o %s" % (
                compilator,
                " ".join(cflags),
                s_incdirs,
                source,
                tgt,
            )
            check_call(cmd, shell=True)


if __name__ == "__main__":
    generateTypeHeaders(
        "pyFramework/data_struct", out_file="pyFramework/include/pyFramework_ds"
    )

    generateFunctionHeaders(
        "pyFramework/data_struct",
        pkg_name="pyFramework",
        out_file="pyFramework/pyFramework.py",
    )

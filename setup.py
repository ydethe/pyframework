import os
import codecs
import setuptools
from setuptools.config import read_configuration
import distutils.cmd
import distutils.log
import subprocess
import shutil
import pathlib

from setuptools import setup, Extension

# from numpy.distutils.core import setup, Extension

from pyf.process import generateTypeHeaders, generateFunctionHeaders, compile


def load_setup_args(root):
    conf_pth = os.path.join(root, "setup.cfg")
    conf_dict = read_configuration(conf_pth)

    with codecs.open(
        os.path.join(root, "requirements.txt"),
        mode="r",
        encoding="utf-8",
    ) as f:
        req = f.read().strip().split("\n")

    opts = dict()
    opts.update(conf_dict["option"])
    opts.update(conf_dict["metadata"])
    opts["install_requires"] = req

    return opts


class BuildSphinxCommand(distutils.cmd.Command):
    description = "build sphinx documentation"
    user_options = []

    def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        pass

    def finalize_options(self):
        """Post-process options."""
        pass

    def run_command(self, command):
        p = subprocess.Popen(command)
        p.wait()

    def run(self):
        """Run command."""
        # sphinx-build -T -b html -D language=fr . _build/html
        command = ["sphinx-build"]
        command.append("-T")
        command.append("-b")
        command.append("html")
        command.append("-D")
        command.append("language=fr")
        command.append(".")
        command.append("_build/html")
        self.announce("Running command: %s" % str(command), level=distutils.log.INFO)
        wd = os.getcwd()
        os.chdir("docs")
        os.makedirs("_build", exist_ok=True)
        self.run_command(command)
        os.chdir(wd)


def liste_sources(root, filt_ext):
    l_src = []
    for dirpath, dirnames, filenames in os.walk(root):
        for fic in filenames:
            bn, ext = os.path.splitext(fic)
            if ext.lower() == filt_ext and not bn.endswith("_wrap"):
                l_src.append(os.path.join(dirpath, fic))

    return l_src


root = os.path.dirname(os.path.abspath(__file__))
opts = load_setup_args(root)
pkg_name = opts["name"]
root = os.path.join(root, pkg_name)

generateTypeHeaders(
    "%s/data_struct" % pkg_name, out_file="%s/include/%s_ds" % (pkg_name, pkg_name)
)
generateTypeHeaders(
    "%s/data_struct" % pkg_name, out_file="%s/src/%s_ds" % (pkg_name, pkg_name)
)
generateFunctionHeaders(
    "%s/data_struct" % pkg_name,
    pkg_name=pkg_name,
    out_file="%s/%s.py" % (pkg_name, pkg_name),
)

l_csrc = liste_sources(root, filt_ext=".c")
compile(
    compilator="gcc",
    sources=l_csrc,
    incdirs=["%s/include" % pkg_name],
    destdir="build",
    cflags=["`python3-config --cflags`"],
)

l_fsrc = liste_sources(root, filt_ext=".f90")
compile(
    compilator="gfortran",
    sources=l_fsrc,
    incdirs=["%s/include" % pkg_name],
    destdir="build",
    cflags=["-O2", "-Wall", "-fPIC", "-ffree-form"],
)

if len(l_fsrc) + len(l_csrc) > 0:
    tgt = "%s/_%s.so" % (pkg_name, pkg_name)
    cmd = "gcc -shared `python3-config --ldflags` -o %s build/*.o" % tgt
    print(cmd)
    os.popen(cmd)

if __name__ == "__main__":
    setup(
        **opts,
        ext_package=pkg_name,
        cmdclass={
            "doc": BuildSphinxCommand,
        },
    )
